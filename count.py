#!/usr/bin/env python3
# coding: utf-8

import pygame
import random
import os

def generateQuestion(images):
    image1 = images[random.randint(0, len(images)-1)]
    image1Surface = pygame.image.load(image1)
    if (image1Surface.get_width() > 100):
        image1Surface = pygame.transform.scale_by(image1Surface, 100/image1Surface.get_width())
    if (image1Surface.get_height() > 150):
        image1Surface = pygame.transform.scale_by(image1Surface, 150/image1Surface.get_height())
    N = random.randint(1, 10)
    r = 0
    if (N > 5):
        r = N - 5
    elif (N > 1):
        r = random.randint(1, N-1)
    return (image1Surface, N, r)

def main():
    # Settings
    width = 800
    height = 600
    color_background = (255, 255, 255)
    color_font = (0, 0, 0)
    clock = pygame.time.Clock()

    # images
    images = [ "/usr/share/bambam/data/alien1.gif",
               "/usr/share/bambam/data/bear.gif",
               "/usr/share/bambam/data/dog.gif",
               "/usr/share/bambam/data/frog.gif",
               "/usr/share/bambam/data/tux.gif" ]

    image1Surface, N, r = generateQuestion(images)
    
    # Init
    pygame.init()
    window_size = (width, height)
    screen = pygame.display.set_mode(window_size)

    # Run
    running = True
    while running:
        clock.tick(20)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                break
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_q:
                    running = False
                    break
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_q:
                    running = False
                    break
                image1Surface, N, r = generateQuestion(images)
        
        if (not running):
            break

        # Fills
        screen.fill(color_background)
        for i in range(N-r):
            screen.blit(image1Surface, image1Surface.get_rect(center=((width/(N-r+1))*(i+1), 200)))
        for i in range(r):
            screen.blit(image1Surface, image1Surface.get_rect(center=((width/(r+1))*(i+1), 400)))

        # Updates
        pygame.display.update()

    pygame.quit()

    
if __name__ == "__main__":
    main()
