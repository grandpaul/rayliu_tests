#!/usr/bin/env python3
# coding: utf-8

import pygame
import random

def main():
    # Settings
    width = 800
    height = 600
    color_background = (255, 255, 255)
    color_font = (0, 0, 0)
    clock = pygame.time.Clock()

    # Init
    pygame.init()
    window_size = (width, height)
    screen = pygame.display.set_mode(window_size)

    # Text
    font = pygame.font.SysFont("vlgothic", 300)
    textStr = "O123456789"
    text = []
    for t in textStr:
        text.append(font.render(t, True, color_font))

    text_i = random.randint(0, len(text)-1)
    counter = 0

    # Run
    running = True
    while running:
        clock.tick(20)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                break
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_q:
                    running = False
                    break
                text_i_new = random.randint(0, len(text)-1)
                if (text_i_new == text_i):
                    text_i = (text_i + 1) % len(text)
                else:
                    text_i = text_i_new
                counter = counter + 1

        if (not running):
            break
                
        # Fills
        screen.fill(color_background)
        text1 = text[text_i]
        screen.blit(text1, text1.get_rect(center=(width/2, height/2)))

        # Updates
        pygame.display.update()

    pygame.quit()

if __name__ == "__main__":
    main()
